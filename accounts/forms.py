from django import forms
from .models import Students


'''
@type {Form} Login
@desc Use this form class for generate student login form.
'''


class AccountLoginForms(forms.ModelForm):
    email = forms.EmailField(
            required=True,
            max_length=100,
            widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Type user email ...'})
    )
    password = forms.CharField(
        required=True,
        widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Type password'})
    )

    class Meta():
        model = Students
        fields = ['email', 'password']


'''
@type {Form} User Register
@desc Use this form class for generate student register form.
'''


class RegisterForms(forms.ModelForm):
    first_name = forms.CharField(
        required=True,
        widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'First Name'})
    )
    last_name = forms.CharField(
        required=True,
        widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Last Name'})
    )
    email = forms.EmailField(
        required=True,
        widget=forms.EmailInput(attrs={'class': 'form-control', 'placeholder': 'User Email'})
    )
    password = forms.CharField(
        required=True,
        widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Write Password'})
    )

    class Meta():
        model = Students
        fields = ['first_name', 'last_name', 'email', 'password']
