from django.shortcuts import render, HttpResponseRedirect
from .forms import AccountLoginForms, RegisterForms
from .models import Students

context = dict()


def registration(request):
    context['form'] = RegisterForms()

    if request.method == 'POST':
        form_data = RegisterForms(request.POST)
        if form_data.is_valid():
            form = Students(
                first_name=form_data.cleaned_data['first_name'],
                last_name=form_data.cleaned_data['last_name'],
                email=form_data.cleaned_data['email'],
                password=form_data.cleaned_data['password']
            )
            form.save()
            return HttpResponseRedirect('/accounts/login/')
    return render(request, 'accounts/register/registration.html', context)


def login(request):
    context['form'] = AccountLoginForms()
    if request.method == 'POST':
        form_data = AccountLoginForms(request.POST)
        if form_data.is_valid():
            form = Students(
                        password=form_data.cleaned_data['password'],
                        email=form_data.cleaned_data['email'],
                        first_name='First Name',
                        last_name='Last Name'
                    )
            form.save()
            return HttpResponseRedirect('accounts/login/')
    return render(request, 'accounts/login/login.html', context)

