from django.db import models


class Students(models.Model):
    password = models.CharField(max_length=255)
    email = models.EmailField(max_length=100)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    created_on = models.DateTimeField(auto_now_add=True)


