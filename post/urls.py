from django.urls import path
from post import views

urlpatterns = [
   path('', views.post_index, name="all_post"),
   path('<int:pk>/', views.post_detail, name="post_detail"),
]
