from django.shortcuts import render
from post.models import Post

context = {
    'menu': 'post'
}


# display all post data
def post_index(request):
    # get all post data
    posts = Post.objects.all()
    # create a post data dictionary
    context['posts'] = posts
    return render(request, 'post/post.html', context)


def post_detail(request, pk):
    # get post data using primary key
    post = Post.objects.get(pk=pk)
    # assign post data to the dictionary
    context['post'] = post
    return render(request, 'post/post_detail.html', context)
