from django.shortcuts import render, HttpResponseRedirect
from blog.models import Post, Comment, Category
from blog.forms import CommentForm, BlogCategoryForm
context = {
    'menu': 'blog'
}


# Create your views here.
def blog_index(request):
    posts = Post.objects.all().order_by('created_on')
    # create a blog data dictionary
    context['blogs'] = posts
    return render(request, 'blog/blog_index.html', context)


def blog_details(request, pk):
    post = Post.objects.get(pk=pk)

    form = CommentForm()
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = Comment(
                author=form.cleaned_data['author'],
                body=form.changed_data['body'],
                post=post
            )
            comment.save()

    comments = Comment.objects.filter(post=post)
    context['details'] = post
    context['comments'] = comments
    context['form'] = form
    return render(request, 'blog/details.html', context)


def blog_category(request, category):
    posts = Post.objects.filter(
        categories__name__contains=category
    ).order_by(
        '-created_on'
    )
    context['category'] = category
    context['posts'] = posts
    return render(request, 'blog/blog_category.html', context)


def add_category(request):
    if request.method == 'POST':
        form = BlogCategoryForm(request.POST, request.FILES)
        if form.is_valid():
            category = Category(name=form.cleaned_data['name'], images=request.FILES['images'])
            category.save()
            # cat_name = form.cleaned_data['name']
            # Category.objects.create(name=cat_name)
            return HttpResponseRedirect('/blog/add/category/')
    form = BlogCategoryForm()
    context['form'] = form
    return render(request, 'blog/add_category.html', context)


def user_register(request):
    return render(request, 'blog/user_add.html', context)
