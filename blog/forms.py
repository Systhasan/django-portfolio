from django import forms
from .models import Category


class CommentForm(forms.Form):
    author = forms.CharField(
        max_length=60,
        widget=forms.TextInput(attrs={
            "class": "form-control",
            "placeholder": "Your Name..."
        })
    )
    body = forms.CharField(widget=forms.Textarea(
        attrs={
            "class": "form-control",
            "placeholder": "Leave a comment..."
        }
    ))


class BlogCategoryForm(forms.ModelForm):
    name = forms.CharField(
            required=True, max_length=100,
            widget=forms.TextInput(
                attrs={
                        'class': 'form-control',
                        'placeholder': 'Enter category'
                    }
            )
        )
    images = forms.FileField(label='Select a file', help_text='Max. 40MB')

    class Meta():
        model = Category
        fields = ['name', 'images']
