from django.urls import path
from blog import views

urlpatterns = [
   path('', views.blog_index, name="all_blog"),
   path('<int:pk>/', views.blog_details, name="blog_details"),
   path('<category>/', views.blog_category, name="blog_category"),
   path('add/user/', views.user_register, name="user-accounts"),
   path('add/category/', views.add_category, name="add_category")
]
