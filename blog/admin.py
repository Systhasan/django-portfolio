from django.contrib import admin
from blog.models import Post, Category

# Register your models here.


class PostAdmin(admin.ModelAdmin):
    pass


class CategoryAdmin(admin.ModelAdmin):
    pass


# Register apps to the admin.
admin.site.register(Post, PostAdmin)
admin.site.register(Category, CategoryAdmin)
